//
//  HealthStatus.swift
//  com.souhailtest.normalApp
//
//  Created by Souhail Marghabi on 04.11.18.
//  Copyright © 2018 Souhail Marghabi. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

protocol Retrievable {
    static func url() -> String
}

class HealthStatus: Object, Mappable, Retrievable {
    @objc dynamic var statusType: ServerStatus.RawValue = ""
    @objc dynamic var timeStamp: String? // Date
    @objc dynamic var sessionID = UUID().uuidString

    override static func primaryKey() -> String? {
        return "sessionID"
    }

    enum ServerStatus: String {
        case UP
        case DOWN
    }

    required convenience init?(map _: Map) {
        self.init()
    }

    func mapping(map: Map) {
        statusType <- (map["status"])
    }

    static func url() -> String {
        let serverURL = EnvironmentConfiguration().configuration(PlistKey.serverURL)

        return serverURL
    }
}

extension HealthStatus {
    static func all() -> Results<HealthStatus> {
        let realm = try! Realm()
        return realm.objects(HealthStatus.self)
            .sorted(byKeyPath: "timeStamp", ascending: false)
    }

}
