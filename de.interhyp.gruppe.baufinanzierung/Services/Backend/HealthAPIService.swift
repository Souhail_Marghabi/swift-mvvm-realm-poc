//
//  APIManager.swift
//  com.souhailtest.normalApp
//
//  Created by Souhail Marghabi on 04.11.18.
//  Copyright © 2018 Souhail Marghabi. All rights reserved.
//

import UIKit

import Alamofire
import AlamofireObjectMapper
import Foundation
import ObjectMapper
import RealmSwift

protocol HealthServiceProtocol: class {
    func GetSystemStatus(type: String, success: @escaping (_ didsucceed: Object) -> Void, fail: @escaping (_ error: NSError) -> Void)
}

class HealthAPIService: HealthServiceProtocol {

    static let shared = HealthAPIService()

    let sessionManager = Alamofire.SessionManager.default

    var sessiontasks: [URLSessionDataTask]?

    // Trying out canceling different type of http requests specific to our API
    func cancelHealthStatsRequests() {

        sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in

            dataTasks.forEach {
                if ($0.originalRequest?.url?.absoluteString == HealthStatus.url()) {
                    $0.cancel()
                }
            }

            //For future purposes
            uploadTasks.forEach { $0.cancel() }
            downloadTasks.forEach { $0.cancel() }
        }
    }

    func GetSystemStatus(type: String, success: @escaping (Object) -> Void, fail: @escaping (NSError) -> Void) {
        Alamofire.request(HealthStatus.url(), method: .get).responseJSON { response in
            print(response)
            //to get status code
            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd hh:mm:ssss.SSSS"
            let now = df.string(from: Date())

            switch response.result {
            case let .success(item):

                guard let data = Mapper<HealthStatus>().map(JSONObject: item)
                    else {
                        print("could not parse response")
                        return
                }

                data.timeStamp = now
                print(response.result)
                success(data)
            case let .failure(error):
                fail(error as NSError)
            }
        }
    }

}
