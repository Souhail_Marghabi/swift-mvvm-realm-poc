//
//  PersistenceHelper.swift
//  com.souhailtest.normalApp
//
//Realm Persistence Manager
//
//  Created by Souhail Marghabi on 07.11.18.
//  Copyright © 2018 Souhail Marghabi. All rights reserved.
//

import Foundation
import RealmSwift

class PersistenceService {
    let uiRealm = try! Realm()

    private init() {}

    public static let shared = PersistenceService()

    public func loadItems() -> [HealthStatus] {
        return Array(uiRealm.objects(HealthStatus.self)).sorted(by: {
            $0.timeStamp?.compare(($1.timeStamp)!) == .orderedDescending
        })
    }

    public func addNewItem(repo: HealthStatus) {
        try! uiRealm.write {
            uiRealm.add(repo)
        }
    }

    public func addLists(list: [HealthStatus]) {
        try! uiRealm.write {
            list.forEach { item in
                if uiRealm.object(ofType: HealthStatus.self, forPrimaryKey: item.sessionID) != nil {
                    uiRealm.create(HealthStatus.self, value: item, update: true)
                } else {
                    uiRealm.add(item)
                }
            }
        }
    }

    public func clearAllItems() {
        try! uiRealm.write {
            uiRealm.delete(uiRealm.objects(HealthStatus.self))
        }
    }

    public func deleteItem(item: HealthStatus) {
        try! uiRealm.write {
            uiRealm.delete(item)
        }
    }
}

extension Results {
    func toArray<T>(type: T.Type) -> [T] {
        return compactMap { $0 as? T }
    }
}
