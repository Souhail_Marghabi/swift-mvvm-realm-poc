//
//  EnvironmentConfiguration.swift
//  com.souhailtest.normalApp
//
//  Created by Souhail Marghabi on 04.11.18.
//  Copyright © 2018 Souhail Marghabi. All rights reserved.
//

import Foundation

public enum PlistKey {
    case serverURL
    case timeoutInterval
    case connectionProtocol

    func value() -> String {
        switch self {
        case .serverURL:
            return "API_BASE_URL_ENDPOINT"
        case .timeoutInterval:
            return "TIMEOUT_INTERVAL"
        case .connectionProtocol:
            return "PROTOCOL"
        }
    }
}

private func UITesting() -> Bool {
    return ProcessInfo.processInfo.arguments.contains("UI-TESTING")
}


public struct EnvironmentConfiguration {

    fileprivate var infoDict: [String: Any] {
        if let dict = Bundle.main.infoDictionary {
            return dict
        } else {
            fatalError("Plist file not found")
        }
    }

    public func configuration(_ key: PlistKey) -> String {
        switch key {
        case .serverURL:
            #if DEBUG
            enum Environment: String {
                case production = "https://helix.interhyp.de/management/health"
                case debug = "https://helix-test.interhyp.de/management/health"
                case staging = "https://helix-livetest.interhyp.de/management/health"
                case localhost = "http://localhost:8098/management/health"
            }

            return Environment.production.rawValue //we want to have the flexibility to change that during development
            #else
            return infoDict[PlistKey.serverURL.value()] as! String
            #endif

        case .timeoutInterval:
            return infoDict[PlistKey.timeoutInterval.value()] as! String
        case .connectionProtocol:
            return infoDict[PlistKey.connectionProtocol.value()] as! String
        }
    }

}
