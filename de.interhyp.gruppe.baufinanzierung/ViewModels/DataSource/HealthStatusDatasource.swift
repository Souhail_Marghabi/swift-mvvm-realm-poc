//
//  HealthStatusDatasource.swift
//  com.souhailtest.normalApp
//
//  Created by Souhail Marghabi on 07.11.18.
//  Copyright © 2018 Souhail Marghabi. All rights reserved.
//

import Foundation
import UIKit

class GenericDataSource<T>: NSObject {
    var data: DynamicValue<[T]> = DynamicValue([])
}

class HealthStatusDatasource: GenericDataSource<HealthStatus>, UITableViewDataSource {
    // MARK: - Table View
    private let realmHelper = PersistenceService.shared
    
    func numberOfSections(in _: UITableView) -> Int {
        return 1
    }
    
    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return data.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let stati = data.value[indexPath.row]
        let itemTimeStamp = stati.timeStamp
        let itemSystemStatus = stati.statusType
        
        if let itemTimeStamp = itemTimeStamp {
            cell.textLabel!.text = itemTimeStamp + " " + itemSystemStatus
            return cell
        } else {
            print("no value found")
            return cell
        }
        
    }
    
    func tableView(_: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if let item = data.value[indexPath.row] as HealthStatus? {
                self.realmHelper.deleteItem(item: item)
                data.value.remove(at: indexPath.row)
            }
        }
    }
}
