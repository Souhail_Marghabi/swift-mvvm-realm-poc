//
//  HealthStatusViewModel.swift
//  com.souhailtest.normalApp
//
//  Created by Souhail Marghabi on 07.11.18.
//  Copyright © 2018 Souhail Marghabi. All rights reserved.
//

import Foundation
import RealmSwift

struct HealthStatusViewModel {
    private var refreshToken: NotificationToken?
    private(set) var retrievedStatus: Results<HealthStatus>?

    weak var dataSource: GenericDataSource<HealthStatus>?
    weak var service: HealthServiceProtocol?
    weak var persistenceService: PersistenceService?

    init(backendservice: HealthServiceProtocol = HealthAPIService.shared, persistence: PersistenceService = PersistenceService.shared, dataSource: GenericDataSource<HealthStatus>?) {
        self.dataSource = dataSource
        self.service = backendservice
        self.persistenceService = persistence
    }

    func fetchPersistedResponses() -> [HealthStatus]? {
        guard let persistenceService = persistenceService else {
            print("missing persistence service")
            return nil
        }

        return persistenceService.loadItems()
    }

    func fetchHealthStatus(success: @escaping () -> Void, fail: @escaping (NSError) -> Void) {

        let error = ServiceError.unexpected as NSError
        guard let service = service else {
            print("missing service")
            fail(error)
            return
        }

        guard persistenceService != nil else {
            print("missing peristence service")
            fail(error)
            return
        }

        service.GetSystemStatus(type: HealthStatus.url(), success: {
            result in
            self.persistenceService?.addNewItem(repo: result as! HealthStatus)
            self.dataSource?.data.value = (self.persistenceService?.loadItems())!
            success()
        }, fail: {
            Error in
            fail(Error)
        })
    }

}

extension HealthStatusViewModel {

    public mutating func didFinishUpdate( updated: @escaping (RealmCollectionChange<Results<HealthStatus>>) -> Void) {
        refreshToken?.invalidate()
        retrievedStatus = HealthStatus.all()
        refreshToken = retrievedStatus?.observe(updated)

    }
}
