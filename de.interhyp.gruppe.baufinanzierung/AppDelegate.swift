//
//  AppDelegate.swift
//  de.interhyp.gruppe.baufinanzierung
//
//  Created by Souhail Marghabi on 05.11.18.
//  Copyright © 2018 Souhail Marghabi. All rights reserved.
//

import AppCenter
import AppCenterAnalytics
import AppCenterCrashes
import UIKit
import AlamofireNetworkActivityLogger


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate {
    var window: UIWindow?

    class func shared() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }

    fileprivate func setNavigationBarAppearance() {
        let navigationBarAppearace = UINavigationBar.appearance()
    
        navigationBarAppearace.tintColor = UIColor.white
        navigationBarAppearace.barTintColor = UIColor(hexRGB: "#ff7518")
        
        // change navigation item title color in appdelegate because we have one screen in this case
        navigationBarAppearace.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }

    func application(_: UIApplication, didFinishLaunchingWithOptions _: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        MSAppCenter.start("8a222479-4e92-4a7a-9007-3f9c33cd57a6", withServices: [MSAnalytics.self, MSCrashes.self])

        // Override point for customization after application launch.
        
        if let splitViewController = window!.rootViewController as? UISplitViewController {
            let navigationController = splitViewController.viewControllers[splitViewController.viewControllers.count - 1] as! UINavigationController
            navigationController.topViewController!.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem
            
            splitViewController.delegate = self

        }
        
        
    
        setNavigationBarAppearance()

        NetworkActivityLogger.shared.startLogging()
        return true
    }


    // MARK: - Split view

    func splitViewController(_: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto _: UIViewController) -> Bool {
        guard let secondaryAsNavController = secondaryViewController as? UINavigationController else { return false }
        guard let topAsDetailController = secondaryAsNavController.topViewController as? DetailViewController else { return false }
        if topAsDetailController.detailItem == nil {
            // Return true to indicate that we have handled the collapse by doing nothing; the secondary controller will be discarded.
            return true
        }
        return false
    }

    // MARK: - BuildConfig Check

    func isRunningUnitTests() -> Bool {
        let env = ProcessInfo.processInfo.environment
        if env["XCTestConfigurationFilePath"] != nil {
            return true
        }
        return false
    }

}
