//
//  BackendLocalizedErrorType.swift
//  de.interhyp.gruppe.baufinanzierung
//
//  Created by Souhail Marghabi on 21.11.18.
//  Copyright © 2018 Souhail Marghabi. All rights reserved.
//

import UIKit

enum ServiceError: LocalizedError {
    case expected
    case unexpected

    var errorDescription: String? {
        var errorString: String?
        switch self {
        case .expected:
            errorString = "cancelled"
        default:
            errorString = localizedDescription
        }

        return errorString
    }
}
