//
//  StartScreenViewController.swift
//  de.interhyp.gruppe.baufinanzierung
//
//  Created by Souhail Marghabi on 04.02.19.
//  Copyright © 2019 Souhail Marghabi. All rights reserved.
//

import UIKit
import TransitionButton

class StartScreenViewController: CustomTransitionViewController, UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // pass any object as parameter, i.e. the tapped row
        performSegue(withIdentifier: "showDetailSegue", sender: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        //let cell = tableView.dequeueReusableCell(withIdentifier: "mockCell", for: indexPath)
        let cell1 = tableView.dequeueReusableCell(withIdentifier: "mockCell")
        let cell2 = tableView.dequeueReusableCell(withIdentifier: "demoCell")
        let cell3 =  tableView.dequeueReusableCell(withIdentifier: "lastCell")

        if(indexPath.row == 0) {
            return cell1!
        }
        if(indexPath.row == 1) {
            return cell2!
        }
        if(indexPath.row == 2) {
            return cell3!
        }

        return cell3!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170.0
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.prefersLargeTitles = true
        // Do any additional setup after loading the view.
    }

    /*
     // MARK: - Navigation

     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */

}
