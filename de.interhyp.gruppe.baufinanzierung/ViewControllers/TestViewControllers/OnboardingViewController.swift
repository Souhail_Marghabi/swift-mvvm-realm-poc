//
//  OnboardingViewController.swift
//  de.interhyp.gruppe.baufinanzierung
//
//  Created by Souhail Marghabi on 05.02.19.
//  Copyright © 2019 Souhail Marghabi. All rights reserved.
//

import UIKit
import paper_onboarding

class OnboardingViewController: UIViewController {

    @IBOutlet weak var skipButton: UIButton!
    
    fileprivate let items = [
        OnboardingItemInfo(informationImage: Asset.banks.image,
                           title: "Interhyp Baufinanzierung",
                           description: "Am Anfang jeder Immobilienfinanzierung steht eine ausführliche Analyse.",
                           pageIcon: Asset.key.image,
                           color: UIColor(hexRGB: "#ff7518")!,
                           titleColor: UIColor.white, descriptionColor: UIColor.white, titleFont: titleFont, descriptionFont: descriptionFont),
        
        OnboardingItemInfo(informationImage: Asset.shoppingCart.image,
                           title: "Individuelles Gesamtkonzept",
                           description: "Mit diesem Wissen setzt Ihr persönlicher Interhyp-Berater die entscheidenden Bausteine zu einem grundlegenden Gesamtkonzept zusamme.",
                           pageIcon: Asset.wallet.image,
                           color: UIColor(red: 0.40, green: 0.69, blue: 0.71, alpha: 1.00),
                           titleColor: UIColor.white, descriptionColor: UIColor.white, titleFont: titleFont, descriptionFont: descriptionFont),
        
        OnboardingItemInfo(informationImage: Asset.wallet.image,
                           title: "Passendes Angebot",
                           description: "Ihr persönlicher Berater wird Ihnen in der Regel zwei bis drei Angebote verschiedener Institute vorstellen, die Ihren Bedürfnissen bestmöglich entsprechen",
                           pageIcon: Asset.shoppingCart.image,
                           color: UIColor.darkText,
                           titleColor: UIColor.white, descriptionColor: UIColor.white, titleFont: titleFont, descriptionFont: descriptionFont),
        
        ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        skipButton.isHidden = true
        
        setupPaperOnboardingView()
        
        view.bringSubviewToFront(skipButton)
    }
    
    private func setupPaperOnboardingView() {
        let onboarding = PaperOnboarding()
        onboarding.delegate = self
        onboarding.dataSource = self
        onboarding.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(onboarding)
        
        // Add constraints
        for attribute: NSLayoutConstraint.Attribute in [.left, .right, .top, .bottom] {
            let constraint = NSLayoutConstraint(item: onboarding,
                                                attribute: attribute,
                                                relatedBy: .equal,
                                                toItem: view,
                                                attribute: attribute,
                                                multiplier: 1,
                                                constant: 0)
            view.addConstraint(constraint)
        }
    }
}

// MARK: Actions

extension OnboardingViewController {
    
    @IBAction func skipButtonTapped(_: UIButton) {
        print(#function)
    }
}

// MARK: PaperOnboardingDelegate

extension OnboardingViewController: PaperOnboardingDelegate {
    
    func onboardingWillTransitonToIndex(_ index: Int) {
        skipButton.isHidden = index == 2 ? false : true
    }
    
    func onboardingDidTransitonToIndex(_: Int) {
    }
    
    func onboardingConfigurationItem(_ item: OnboardingContentViewItem, index: Int) {
        item.titleLabel?.lineBreakMode = .byWordWrapping
        item.titleLabel?.numberOfLines = 3
        //item.descriptionLabel?.backgroundColor = .redColor()
        //item.imageView = ...
    }
}

// MARK: PaperOnboardingDataSource

extension OnboardingViewController: PaperOnboardingDataSource {
    
    func onboardingItem(at index: Int) -> OnboardingItemInfo {
        return items[index]
    }
    
    func onboardingItemsCount() -> Int {
        return 3
    }
    
    //    func onboardinPageItemRadius() -> CGFloat {
    //        return 2
    //    }
    //
    //    func onboardingPageItemSelectedRadius() -> CGFloat {
    //        return 10
    //    }
    //    func onboardingPageItemColor(at index: Int) -> UIColor {
    //        return [UIColor.white, UIColor.red, UIColor.green][index]
    //    }
}


//MARK: Constants
extension OnboardingViewController {
    
    private static let titleFont = UIFont(name: "Nunito-Bold", size: 36.0) ?? UIFont.boldSystemFont(ofSize: 33.0)
    private static let descriptionFont = UIFont(name: "OpenSans-Regular", size: 17.0) ?? UIFont.systemFont(ofSize: 17.0)
}
