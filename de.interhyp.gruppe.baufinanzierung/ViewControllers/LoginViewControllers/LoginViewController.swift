//
//  LoginViewController.swift
//  de.interhyp.gruppe.baufinanzierung
//
//  Created by Souhail Marghabi on 31.01.19.
//  Copyright © 2019 Souhail Marghabi. All rights reserved.
//

import UIKit
import TransitionButton

class LoginViewController: UIViewController {

    @IBOutlet weak var passwordField: SkyFloatingLabelTextField!
    @IBOutlet weak var emailField: SkyFloatingLabelTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        setupKeyboardDismissRecognizer()
        emailField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        // Do any additional setup after loading the view.
    }

    func setupKeyboardDismissRecognizer() {
        let tapRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(dismissKeyboard))

        self.view.addGestureRecognizer(tapRecognizer)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

    @IBAction func onLoginClick(_ sender: TransitionButton) {
        view.endEditing(true)
        sender.startAnimation() // 2: Then start the animation when the user tap the button
        let qualityOfServiceClass = DispatchQoS.QoSClass.background
        let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
        let isnotempty = !(passwordField.text?.isEmpty ?? true)
        let emailValid = (emailField.text?.contains("@") ?? false)

        if(!isnotempty) {
            sender.stopAnimation(animationStyle: .shake, completion: {
                // self.present(secondVC, animated: false, completion: nil)

            })
        } else {
            backgroundQueue.async(execute: {

                sleep(2) // 3: Do your networking task or background work here.

                DispatchQueue.main.async(execute: { () -> Void in
                    sender.stopAnimation(animationStyle: .expand, completion: {
                        let secondVC = UIStoryboard(name: "MainScreen", bundle: nil).instantiateViewController(withIdentifier: "TabMenu") as! UITabBarController
                        self.navigationController?.pushViewController(secondVC, animated: false)

                        // self.present(secondVC, animated: false, completion: nil)

                    })
                })
            })
        }
    }
    /*
     // MARK: - Navigation

     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }


     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */

    @IBAction func didEndEditing(_ sender: UITextField) {
        if let text = sender.text {
            if let floatingLabelTextField = sender as? SkyFloatingLabelTextField {
                if(text.characters.count == 0) {
                    floatingLabelTextField.errorMessage = ""
                }
            }
        }
    }
    @objc func textFieldDidChange(_ textfield: UITextField) {
        if let text = textfield.text {
            if let floatingLabelTextField = textfield as? SkyFloatingLabelTextField {

                if(text.characters.count > 1 && !text.contains("@")) {
                    floatingLabelTextField.errorMessage = "Invalid email"
                } else {
                    // The error message will only disappear when we reset it to nil or empty string
                    floatingLabelTextField.errorMessage = ""
                }
            }
        }
    }

}
