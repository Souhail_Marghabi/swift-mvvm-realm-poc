//
//  MasterViewController.swift
//  de.interhyp.gruppe.baufinanzierung
//
//  Created by Souhail Marghabi on 05.11.18.
//  Copyright © 2018 Souhail Marghabi. All rights reserved.
//

import RealmSwift
import UIKit

class MasterViewController: UITableViewController {
    var detailViewController: DetailViewController?

    var notificationToken: NotificationToken?

    let loadingViewController = LoadingViewController()
    let dataSource = HealthStatusDatasource()

    lazy var viewModel: HealthStatusViewModel = {
        let viewModel = HealthStatusViewModel(dataSource: dataSource)
        return viewModel
    }()

    fileprivate func setupActionTriggers() {
        // Do any additional setup after loading the view, typically from a nib.
        navigationItem.leftBarButtonItem = editButtonItem

        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(fetchStatus(_:)))
        navigationItem.rightBarButtonItem = addButton
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count - 1] as! UINavigationController).topViewController as? DetailViewController
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        if(dataSource.data.value.count == 0) {
            dataSource.data.value = viewModel.fetchPersistedResponses()!
        }

        tableView.dataSource = dataSource

        dataSource.data.addAndNotify(observer: self) { [weak self] _ in
            print(self?.dataSource.data.value ?? "no stored data")
            //self?.tableView.reloadData()
        }

        
        if(!AppDelegate.shared().isRunningUnitTests()) {
            //React on the UIThread on Realm Observed Collection updates
            registerForUpdate()
        }

        setupActionTriggers()
    }

    override func viewWillAppear(_ animated: Bool) {
       // clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    
    override func viewWillDisappear(_ animated: Bool) {
        notificationToken?.invalidate()
        super.viewWillDisappear(animated)
    }

    func alert(title: String, message: String) {
        let actionSheetController: UIAlertController = UIAlertController(title: title,
                                                                         message: message, preferredStyle: .alert)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { _ -> Void in }
        actionSheetController.addAction(cancelAction)
        present(actionSheetController, animated: true, completion: nil)
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender _: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {

                let selectedObject = dataSource.data.value[indexPath.row]
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController

                controller.detailInfo = selectedObject.timeStamp
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }
}

// MARK: - IBAction

extension MasterViewController {
    fileprivate func retrieveSystemStati() {
        viewModel.fetchHealthStatus(success: {[weak self] () -> Void in
            // Success
            self?.loadingViewController.remove()
        }) { [weak self] (error) -> Void in
            // Handle Errors
            self?.alert(title: "Error", message: error.localizedDescription)
            self?.loadingViewController.remove()
        }
    }

    fileprivate func registerForUpdate() {

        viewModel.didFinishUpdate { [weak self] changes in

            switch changes {
            case .initial:
                self?.tableView.reloadData()

            case .update(_, let del, let ins, let upd):
                self?.tableView.beginUpdates()
                self?.tableView.insertRows(at: ins.map {IndexPath(row: $0, section: 0)}, with: .automatic)
                self?.tableView.reloadRows(at: upd.map {IndexPath(row: $0, section: 0)}, with: .automatic)
                self?.tableView.deleteRows(at: del.map {IndexPath(row: $0, section: 0)}, with: .automatic)
                self?.tableView.endUpdates()

            default: break
            }
        }
    }
    @IBAction func fetchStatus(_: Any) {
        add(loadingViewController)
        retrieveSystemStati()
    }
}
