//
//  DetailViewController.swift
//  de.interhyp.gruppe.baufinanzierung
//
//  Created by Souhail Marghabi on 05.11.18.
//  Copyright © 2018 Souhail Marghabi. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    @IBOutlet var detailDescriptionLabel: UILabel!

    func configureView() {
        // Update the user interface for the detail item.
        if let label = detailDescriptionLabel {
            label.text = detailInfo
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configureView()
    }
    
    var detailItem: NSDate? {
        didSet {
            // Update the view.
            configureView()
        }
    }

    var detailInfo: String? {
        didSet {
            // Update the view.
            configureView()
        }
    }
}
