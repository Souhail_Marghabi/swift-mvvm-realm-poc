//
//  MockStatus.swift
//  de.interhyp.gruppe.baufinanzierungTests
//
//  Created by Souhail Marghabi on 19.11.18.
//  Copyright © 2018 Souhail Marghabi. All rights reserved.
//

import Foundation
@testable import de_interhyp_gruppe_baufinanzierung

struct MockStatus : Retrievable{
    static func url() -> String {
        // SHould be retrieved from Build Flavor PList
        let path = EnvironmentConfiguration().configuration(PlistKey.serverURL)
        return path
    }
    
        static var data: [HealthStatus] {
            
            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd hh:mm:ssss.SSSS"
            let now = df.string(from: Date())
            // prepare data values
            let firstItem = HealthStatus()
            firstItem.statusType = "UP"
            firstItem.timeStamp = now
            
            let secondItem = HealthStatus()
            secondItem.statusType = "DOWN"
            secondItem.timeStamp = "2016-10-08 22:31"
            
            return [firstItem, secondItem]
        }
    
}
