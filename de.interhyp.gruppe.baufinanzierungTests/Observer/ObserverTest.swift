//
//  ObserverTest.swift
//  de.interhyp.gruppe.baufinanzierungTests
//
//  Created by Souhail Marghabi on 30.11.18.
//  Copyright © 2018 Souhail Marghabi. All rights reserved.
//

import XCTest
@testable import de_interhyp_gruppe_baufinanzierung

class ObserverTest: XCTestCase {

    func testAddObserver() {
        let bindable = DynamicValue(false)
        
        let expectListenerCalled = expectation(description: "Listener is called")
        
        bindable.addObserver(self) { value in
            XCTAssert(value == true, "testAddObserverfailed, should have been true")
            expectListenerCalled.fulfill()
        }
 
        
        bindable.value = true
        waitForExpectations(timeout: 0.1, handler: nil)
    }
    
    func testAddAndNotify() {
        let bindable = DynamicValue(true)
        
        let expectListenerCalled = expectation(description: "Listener is called")
        bindable.addAndNotify(observer: self) { value in
            XCTAssert(value == true, "testAddAndNotify failed, should have been true")
            expectListenerCalled.fulfill()
        }
            
    waitForExpectations(timeout: 0.1, handler: nil)
    }

}
