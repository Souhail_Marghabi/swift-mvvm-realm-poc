//
//  PersistenceServiceTest.swift
//  de.interhyp.gruppe.baufinanzierungTests
//
//  Created by Souhail Marghabi on 23.11.18.
//  Copyright © 2018 Souhail Marghabi. All rights reserved.
//

import XCTest

import RealmSwift
import ObjectMapper
@testable import de_interhyp_gruppe_baufinanzierung

class PersistenceServiceTest: TestCasesBase {
    let uiRealm = try! Realm()
    override func setUp() {
        super.setUp()
        
        PersistenceService.shared.clearAllItems()
    }
    
    func testMapsData() {
        let jsonDictionary: [String: Any] = ["status": "DOWN"]
        let user = Mapper<HealthStatus>().map(JSON: jsonDictionary)

        XCTAssertEqual(user?.statusType, "DOWN")
    }


    func testAddListPersistedWithService() {
        
        // giving no service to a view model
        
        let newElements = MockStatus.data

        //PersistenceService.shared.addLists(list: newElements)
        
        PersistenceService.shared.addLists(list: newElements)
        // expected to not be able to fetch items
        
        let newList = uiRealm.objects(HealthStatus.self).toArray(type: HealthStatus.self)
        
        //XCTAssert(initialList.append(contentsOf: newElements),"Deletion did not work")
        XCTAssert(newList.containsArray(array: newElements),"Elements added success")
        
        
    }
    
    func testDeletePersistedWithService() {
        
        // giving no service to a view model

        let newElements = MockStatus.data
        let firstItem = newElements.first
        
        //PersistenceService.shared.addLists(list: newElements)
    
        try! uiRealm.write {
            uiRealm.add(firstItem!)
        }
        // expected to not be able to delete items
        
        PersistenceService.shared.deleteItem(item: firstItem!)

        XCTAssert((firstItem!.isInvalidated), "the item was really deleted")
        
    }

}

//fileprivate
extension Array where Element: Hashable {
    func containsArray(array: [Element]) -> Bool {
        let selfSet = Set(self)
        return !array.contains { !selfSet.contains($0) }
    }
}
