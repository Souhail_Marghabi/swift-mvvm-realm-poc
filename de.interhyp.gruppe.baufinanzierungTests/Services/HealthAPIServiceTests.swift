//
//  HealthAPIServiceTests.swift
//  de.interhyp.gruppe.baufinanzierungTests
//
//  Created by Souhail Marghabi on 21.11.18.
//  Copyright © 2018 Souhail Marghabi. All rights reserved.
//

import XCTest
@testable import de_interhyp_gruppe_baufinanzierung

class HealthAPIServiceTests: XCTestCase {

    func testCancelRequest() {
        // giving an active session
        HealthAPIService.shared.GetSystemStatus(type: MockStatus.url(), success: {
                _ in
        }, fail: { Error in

             XCTAssert(Error.localizedDescription == ServiceError.expected.errorDescription, " Expected the error of type \(ServiceError.expected) but got \(ServiceError.unexpected)")
        })

        // Expected to task nil after cancel
        HealthAPIService.shared.cancelHealthStatsRequests()
    }

}
