//
//  HealthStatusDatasourceTests.swift
//  de.interhyp.gruppe.baufinanzierungTests
//
//  Created by Souhail Marghabi on 19.11.18.
//  Copyright © 2018 Souhail Marghabi. All rights reserved.
//

import XCTest
@testable import de_interhyp_gruppe_baufinanzierung

class HealthStatusDatasourceTests: XCTestCase {

    var dataSource: HealthStatusDatasource!
    
    override func setUp() {
        super.setUp()
        dataSource = HealthStatusDatasource()
    }
    
    override func tearDown() {
        dataSource = nil
        super.tearDown()
    }
    
    func testEmptyValueInDataSource() {
        // giving empty data value
        dataSource.data.value = []
        
        let tableView = UITableView()
        tableView.dataSource = dataSource
        
        // expected one section
        XCTAssertEqual(dataSource.numberOfSections(in: tableView), 1, "Expected one section in table view")
        
        // expected zero cells
        XCTAssertEqual(dataSource.tableView(tableView, numberOfRowsInSection: 0), 0, "Expected no cell in table view")
    }
    
    func testValueInDataSource() {
        
        // Provide Mock data value
        
        dataSource.data.value = MockStatus.data
        
        let tableView = UITableView()
        tableView.dataSource = dataSource
        
        // expected one section
        XCTAssertEqual(dataSource.numberOfSections(in: tableView), 1, "Expected one section in table view")
        
        // expected two cells
        XCTAssertEqual(dataSource.tableView(tableView, numberOfRowsInSection: 0), 2, "Expected two cell in table view")
    }
    
    func testValueCell() {
        // giving data value
        let sampleObject = HealthStatus()
        
        dataSource.data.value = [sampleObject]
        
        let tableView = UITableView()
        tableView.dataSource = dataSource
        
        let indexPath = IndexPath(row: 0, section: 0)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        
        // expected Standard UITableviewCell class, unecessary because we don t use customCells
        guard let _ = dataSource.tableView(tableView, cellForRowAt: indexPath) as? UITableViewCell else {
            XCTAssert(false, "Expected UITableviewCell class")
            return
        }
    }
}

