//
//  de_interhyp_gruppe_baufinanzierungTests.swift
//  de.interhyp.gruppe.baufinanzierungTests
//
//  Created by Souhail Marghabi on 05.11.18.
//  Copyright © 2018 Souhail Marghabi. All rights reserved.
//

@testable import de_interhyp_gruppe_baufinanzierung
import XCTest
import RealmSwift

class TestCasesBase: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
        // Use an in-memory Realm identified by the name of the current test.
        // This ensures that each test can't accidentally access or modify the data
        // from other tests or the application itself, and because they're in-memory,
        // there's nothing that needs to be cleaned up.

        Realm.Configuration.defaultConfiguration.inMemoryIdentifier = "TestRealm"

    }

}
