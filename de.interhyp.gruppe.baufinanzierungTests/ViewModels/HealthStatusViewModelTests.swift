//
//  HealthStatusViewModelTests.swift
//  de.interhyp.gruppe.baufinanzierungTests
//
//  Created by Souhail Marghabi on 21.11.18.
//  Copyright © 2018 Souhail Marghabi. All rights reserved.
//

import XCTest
import RealmSwift
@testable import de_interhyp_gruppe_baufinanzierung

class HealthStatusViewModelTests: TestCasesBase {
    
    
    var dataSource: GenericDataSource<HealthStatus>?
    fileprivate var backendService: MockBackendService?
    var persistenceService: PersistenceService?
    
    
    var viewModel : HealthStatusViewModel!
    
    
    override func setUp() {
        super.setUp()
        self.backendService = MockBackendService()
        self.dataSource = GenericDataSource<HealthStatus>()
        self.viewModel = HealthStatusViewModel(backendservice: backendService!,persistence:PersistenceService.shared, dataSource: dataSource)
    }
    
    override func tearDown() {
        self.viewModel = nil
        self.dataSource = nil
        self.backendService = nil
        super.tearDown()
    }
    
    func testFetchAPIWithNoService() {
        
        // giving no service to a view model
        viewModel.service = nil
        
        // expected to not be able to fetch article
        viewModel.fetchHealthStatus(success:
            {
                XCTAssert(false, "ViewModel should not be able to fetch without service")
                //success
        }, fail: {_ in
            XCTAssert(true, "Valid Output, should not retrieve without service")
        })
        
    }
    
    func testFetchPersistedWithService(){

        let demoList = viewModel.fetchPersistedResponses()
        XCTAssertNotNil(demoList,"Expects empty or full list")
    }

    func testFetchPersistedWithNoService() {
        
        // giving no service to a view model
        viewModel.persistenceService = nil
        
        // expected to not be able to fetch items as null
        let demoList = viewModel.fetchPersistedResponses()
        XCTAssertNil(demoList,"should not retrieve without service")
        
    }
    
    func testFetchStatus() {
        // expected completion to succeed
        
        viewModel.fetchHealthStatus(success:
            {
                //success
        }, fail: {_ in
            XCTAssert(false, "ViewModel should not be able to fetch without service")
        })
        
    }
    
    func testFetchNoStatus() {
        
        // expected completion to fail
        viewModel.persistenceService = nil
        
        viewModel.fetchHealthStatus(success:
            {
                XCTAssert(false, "ViewModel should not be able to fetch anything")
        }, fail: {_ in
            
        })
    }
}

fileprivate class MockBackendService : HealthServiceProtocol {
    
    func GetSystemStatus(type: String, success: @escaping (Object) -> Void, fail: @escaping (NSError) -> Void) {
        let newData = MockStatus.data.first
        if let newsData = newData {
            success(newsData)
        } else {
            fail(ServiceError.unexpected as NSError)
        }
    }
    
}

