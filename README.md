# Swift MVVM Realm POC

Sample Master/Detail POC iOS projects using a simple system health check API trying out different Swift Programming and architecture Patterns.

Features:
===
- Two storyboards :
    - Main.Storyboard for default use case of Master-Detail API call for current system status with Persistence
    - MainScreen.Storyboard for an alternative entry point of the app with a fluid transitions-based Onboarding Screen,Login Screen and MainScreen
    - Should manually set one or the other as main storyboard in Xcode project settings to launch the corresponding UI

Architecture/Development:
===

- Development:
    - Reactive programming with Observer Pattern and MVVM Architecture (from scratch vs RxSwift)
    - Data Persistence through Realm DB
    - Protocol-oriented programming
    
QA:
===

- Automated Testing:
    - Unit-Tests(Models, View, ViewModels, Services,etc) 
    - UITests in BDD Gerkins Format
    - Automated tests execution with Fastlane + Jenkinsfile 
    - Mocked API through WireMock Integration as Test Build Phase
- Test Report(Coverage, Code Metrics):
    - Test Reports generation with Slather, Fastlane scan
    - Code Quality Metrics (Integration with Sonarqube)
    - Static Analysis (SwiftLint + Lizard)
- Device Cloud Testing:
    - Automated Device Testing through upload of UITests and their execution through Microsoft Appcenter)
    
Deployment:
===
- Beta/Appstore:
    - Fastlane testing, building, archiving IPA per configuration, and managing provisioning certificates
    - Beta Testing through Microsoft Appcenter
    - Automatic upload to appstore
