fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
### setup_wiremock
```
fastlane setup_wiremock
```


----

## iOS
### ios build
```
fastlane ios build
```
Provides the IPA's file name, folder and complete path for a given configuration.

Builds the app for iOS. Optional parameters are 'clean' and 'dysm' which includes symbols.

Parameters: configuration, export_method, [clean], [dsym]

Example: fastlane ios build configuration:Staging export_method:ad-hoc
### ios upload_to_appcenter
```
fastlane ios upload_to_appcenter
```
Uploads an IPA to App Center.

Parameters: configuration, token

Example: fastlane ios upload_to_appcenter configuration:Staging token:1234567890
### ios appcenter_uitests
```
fastlane ios appcenter_uitests
```
Uploads and executes UITests in App Center.
### ios screenshots
```
fastlane ios screenshots
```
Generate new localized screenshots
### ios create_sonar_reports
```
fastlane ios create_sonar_reports
```

### ios metrics
```
fastlane ios metrics
```

### ios all_tests
```
fastlane ios all_tests
```

### ios unit_tests
```
fastlane ios unit_tests
```

### ios ui_tests
```
fastlane ios ui_tests
```


----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
