#!/usr/bin/env groovy

def notifyBitBucket = {
    step([$class: 'StashNotifier', commitSha1: '', credentialsId: 'jenkins_bitbucket', disableInprogressNotification: false, ignoreUnverifiedSSLPeer: false, includeBuildNumberInKey: true, prependParentProjectKey: false, projectKey: '', stashServerBaseUrl: 'https://bitbucket.blablup.de/bitbucket'])
}

def notifyByEmail(boolean notifyAll, String buildResult) {
    if (notifyAll && buildResult != 'UNSTABLE') {
        step([$class: 'Mailer', notifyEveryUnstableBuild: true, recipients: '_blablalbal@dodo.de', sendToIndividuals: true])
    } else {
        final def RECIPIENTS = emailextrecipients([
            [$class: 'DevelopersRecipientProvider'],
            [$class: 'CulpritsRecipientProvider'],
            [$class: 'RequesterRecipientProvider']
        ])
        if (RECIPIENTS != null && !RECIPIENTS.isEmpty()) {
            step([$class: 'Mailer', notifyEveryUnstableBuild: true, recipients: RECIPIENTS])
        } else {
            echo 'No email notification sent!'
        }
    }
}

def isSharedBranch = {
    final branch = env.BRANCH_NAME
    return branch == 'develop' || branch == 'master' || branch.startsWith('release')
}

def isDevelopBranch = {
    final branch = env.BRANCH_NAME
    return branch == 'develop'
}

def isReleaseBranch = {
    final branch = env.BRANCH_NAME
    return branch.startsWith('release')
}

def getBuildConfigurationList = {
    buildConfigurations = ['Debug', 'Beta', 'Staging']
    if (isSharedBranch()) {
        if (isReleaseBranch()) {
            buildConfigurations = ['Release', 'Staging', 'Beta', 'Debug']
        } else {
            buildConfigurations = ['Staging', 'Release', 'Beta', 'Debug']
        }
    }
    return buildConfigurations
}

def getExportMethodList = {
    buildConfigurations = ['development', 'ad-hoc']
    if (isSharedBranch()) {
        if (isReleaseBranch()) {
            buildConfigurations = ['app-store', 'ad-hoc', 'development']
        } else {
            buildConfigurations = ['ad-hoc', 'development']
        }
    }
    return buildConfigurations
}

def fastlaneAction(String action) {
    sh '/usr/local/bin/fastlane ' + action
}

node('MacMini') {
    properties([
        parameters([
            choice(name: 'BUILD_CONFIGURATION', choices: getBuildConfigurationList().join("\n")),
            choice(name: 'EXPORT_METHOD', choices: getExportMethodList().join("\n")),
            booleanParam(name: 'UPLOAD_TO_APPCENTER', defaultValue: isReleaseBranch()),
          	booleanParam(name: 'CLEAN_BEFORE_BUILD', defaultValue: false)
        ]),
        disableConcurrentBuilds()
    ])
    withCredentials([usernamePassword(credentialsId: 'smoke-test-user-bo', passwordVariable: 'proxy_password', usernameVariable: 'proxy_user')]) {
        fastlaneEnvironment = [
            'LANG=en_US.UTF-8',
            'LANGUAGE=en_US.UTF-8',
            'LC_ALL=en_US.UTF-8',
            'FASTLANE_DISABLE_COLORS=1',
            'FASTLANE_SKIP_UPDATE_CHECK=1',
            'FASTLANE_OPT_OUT_USAGE=1',
            'FASTLANE_HIDE_TIMESTAMP=1',
            'PATH+GEMLOCAL=/Users/cvsdep/.gem/ruby/2.3.0/bin',
            'GEM_HOME=/Users/cvsdep/.gem/ruby/2.3.0/bin',
            'COCOAPODS_NO_BUNDLER=1',
            'PATH=/usr/local/bin:' + env.PATH
        ]
    }

    try {
        stage('Checkout') {
          	//Notify bitbucket that build started and is in progress
      		notifyBitBucket()

            echo '==========>>>>>>>>>> Checkout <<<<<<<<<<=========='
            checkout scm
        }

    }
    catch (err) {
        err.printStackTrace()
        currentBuild.result = 'FAILURE'
    }

    echo '==========>>>>>>>>>> Notify BitBucket <<<<<<<<<<=========='
  	if (currentBuild.result == null) {
    	currentBuild.result = 'SUCCESS'
  	}
  	notifyBitBucket()

    echo '==========>>>>>>>>>> Send Email Notification <<<<<<<<<<=========='
    notifyByEmail(isSharedBranch(), currentBuild.result)
}
