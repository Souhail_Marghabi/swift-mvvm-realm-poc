//
//  HomeScreen.swift
//  de.interhyp.gruppe.baufinanzierungUITests
//
//  Created by Souhail Marghabi on 16.01.19.
//  Copyright © 2019 Souhail Marghabi. All rights reserved.
//

import XCTest

var app = XCUIApplication()


enum Homescreen {
    case navigationbar
    case detailScreen
    case addButton
    case deleteButton
    case editButton
    case doneButton
    
    var element: XCUIElement {
        switch self {
            
        case .navigationbar:
            return app.navigationBars["Helix System Health Check"]
        case .detailScreen:
            return app.navigationBars["Detail"].buttons.element(boundBy: 0)
        case .addButton:
            return app.buttons["Add"]
        case .deleteButton:
            return app.buttons["Delete"]
        case .doneButton:
            return app.buttons["Done"]
        case .editButton:
            return app.buttons["Edit"]
        }
        
    }
    
    static func validateIntialState() {
        XCTAssert(Homescreen.navigationbar.element.exists)
        XCTAssert(Homescreen.addButton.element.exists)
        snapshot("HomeScreen shown")
    }
    
    static func requestHealthStati() {
        for _ in 1...3 {
            Homescreen.addButton.element.tap()
        }
    }
    
    static func requestHealthStatus() {
            Homescreen.addButton.element.tap()
    }
    
    static func swipeLeftOnCell(forIndex: Int){
        let tablesQuery = app.tables
        let cellatIndex = tablesQuery.cells.element(boundBy: forIndex)
        cellatIndex.swipeLeft()
        snapshot("Edit Mode Activated")
    }
    
    static func tapOnCellAtRow(forIndex: Int){
        let tablesQuery = app.tables
        let cellatIndex = tablesQuery.cells.element(boundBy: forIndex)
        cellatIndex.tap()
        
        snapshot("Detail Screen Shown")
    }
    
    static func popToMainView(){
        XCTAssert(Homescreen.detailScreen.element.exists)
        Homescreen.detailScreen.element.tap()
        
        snapshot("Navigated Back to HomeScreen")
        
        XCTAssertTrue(Homescreen.navigationbar.element.waitForExistence(timeout: 10))
        
    }
    static func deleteSelectedEntry() {
        Homescreen.deleteButton.element.tap()
    }
}
