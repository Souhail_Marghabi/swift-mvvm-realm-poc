//
//  de_interhyp_gruppe_baufinanzierungUITests.swift
//  de.interhyp.gruppe.baufinanzierungUITests
//
//  Created by Souhail Marghabi on 05.11.18.
//  Copyright © 2018 Souhail Marghabi. All rights reserved.
//

import XCTest


class de_interhyp_gruppe_baufinanzierungUITests: XCTestCase,AutomatedAppNavigation {
    
    var app = XCUIApplication()
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        app = XCUIApplication()
        setupSnapshot(app)
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.

        
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    
    func behaviorDrivenTestCase() {
        givenILaunchHealthCheckApp()
        thenIShouldSeeInitialStateOfApp()
        thenISwipeDownOnList()
        whenIRequestHealthStatus()

        whenISwipeLeftOnFirstRecord()
        thenIDeleteStatusRow()
       
    }
    
    func testDetailViewNavigation() {
        givenILaunchHealthCheckApp()
        thenIShouldSeeInitialStateOfApp()
        wait(forElement: Homescreen.editButton.element, timeout: 5)
        thenIRequestHealthStatusOneTime()
        whenIClickOntheFirstItemInList()
        wait(forElement: Homescreen.detailScreen.element, timeout: 5)
        thenINavigateBackToHome()
    }
    
    func testValidHealthFlow() {
        behaviorDrivenTestCase()
    }
    
}

extension XCTestCase {
    func wait(forElement element: XCUIElement, timeout: TimeInterval) {
        let predicate = NSPredicate(format: "exists == 1")
        
        // This will make the test runner continously evalulate the
        // predicate, and wait until it matches.
        expectation(for: predicate, evaluatedWith: element)
        waitForExpectations(timeout: timeout)
    }
}
