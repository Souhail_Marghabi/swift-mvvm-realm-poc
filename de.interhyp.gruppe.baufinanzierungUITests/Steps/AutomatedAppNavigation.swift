//
//  AutomatedAppNavigation.swift
//  de.interhyp.gruppe.baufinanzierungUITests
//
//  Created by Souhail Marghabi on 16.01.19.
//  Copyright © 2019 Souhail Marghabi. All rights reserved.
//

import XCTest

protocol AutomatedAppNavigation {
    func testValidHealthFlow()
    func testDetailViewNavigation()
}

func givenILaunchHealthCheckApp() {
    
    XCTContext.runActivity(named: "Given that I Launched Health Check app") { _ in
        XCUIApplication().launch()
    }
}

 func thenISwipeDownOnList(){
    XCTContext.runActivity(named: "Then I Should Scroll Down in List") { _ in
        XCUIApplication().swipeDown()
    }
    
}
func thenIShouldSeeInitialStateOfApp() {
    XCTContext.runActivity(named: "Then I Should see initial state of  HomeScreen") { _ in
        Homescreen.validateIntialState()
    }
}

func thenIRequestHealthStatusOneTime() {
    
    XCTContext.runActivity(named: "When I sequentially request realtime Health Status") { _ in
        Homescreen.requestHealthStatus()
    }
}

func whenIRequestHealthStatus() {
    
    XCTContext.runActivity(named: "When I sequentially request realtime Health Status") { _ in
        Homescreen.requestHealthStati()
    }
}

func thenIDeleteStatusRow() {
    
    XCTContext.runActivity(named: "Then I delete the selected Health Status row") { _ in
        Homescreen.deleteSelectedEntry()
    }
}
func whenISwipeLeftOnFirstRecord(){
    
    XCTContext.runActivity(named: "When I swipe left on first status of List") { _ in
        Homescreen.swipeLeftOnCell(forIndex: 0)
    }
}

func whenISwipeLeftOnSecondRecord(){
    
    XCTContext.runActivity(named: "When I swipe left on second record of List") { _ in
        Homescreen.swipeLeftOnCell(forIndex: 1)
    }
}

func thenInitialScreenAppears(){
    
    XCTContext.runActivity(named: "Then HomeScreen should appear") { _ in
        Homescreen.validateIntialState()
    }
}

func thenINavigateBackToHome(){
    
    XCTContext.runActivity(named: "When I navigate back to HomeScreen") { _ in
        Homescreen.popToMainView()
    }
}

func whenIClickOntheFirstItemInList(){
    
    XCTContext.runActivity(named: "When I click on first status record in list") { _ in
        Homescreen.tapOnCellAtRow(forIndex: 0)
    }
}

func whenIClickOntheSecondItemInList(){
    
    XCTContext.runActivity(named: "When I click on second status record in list") { _ in
        Homescreen.tapOnCellAtRow(forIndex: 1)
    }
}
